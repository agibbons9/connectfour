package com.oreillyauto;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Your task is to complete the game "Connect Four."
 * - You are provided a skeleton to speed up the program completion.
 * - Please refer to https://www.wikihow.com/Play-Connect-4 for 
 * - game rules. 
 * - Use X to represent the Red Player
 * - Use Y to represent the Blue Player
 * - Reference Helper.java as some of the helper (stubbed) functions 
 *   will assist (speed up) your development.
 */

public class ConnectFour {
    public boolean gameOver = false;           // Game Over boolean
    public boolean gameStarted = false;        // Game Started boolean
    public static BufferedReader consoleReader;// Console Reader to take input from user
    public int attempts = 0;                   // Number of tries/attempts for both players (42 max)
    public String message = "";                // Message to the user 
    public String option = "";                 // Current selected user option (Play Game, Quit etc...)
    public ConnectFour connectFour;            // Class object
    public String turn = "X";                  // Starts with Red Player by default

    
    public final int ROW_COUNT = 6;
    public final int COL_COUNT = 7;
    public final int SPACE_COUNT = ROW_COUNT * COL_COUNT;
    
    public final String EMPTY_SPACE = " ";
    
    public String winner = EMPTY_SPACE;
    
    public String board[] = new String[SPACE_COUNT];
    
    /**
     * Connect Four - Constructor
     * The connect four constructor initializes the game and places
     * the user in a game loop. 
     */
    public ConnectFour() {
        // Declare Variables
        // Get access to the ConnectFour class we created via "this" because
        // we pass the connectFour object to the Helper class in some cases.
        connectFour = this; 
        
        wipeBoard();
        
        // BufferedReader is synchronous while Scanner is not. 
        // BufferedReader has significantly larger buffer memory than Scanner.
        consoleReader = new BufferedReader(new InputStreamReader(System.in)); 
               
        // Set initial message to the user
        Helper.setMessage(option, gameOver, gameStarted, connectFour);
        
        // Game Loop - draw the board, get user selection, and provide feedback
        while (true) {
            // Draw the board (menu, connectfour area)
            drawBoard();
            
            // Get the user selection
            Object userSelection = Helper.getUserOption(consoleReader);
            
            // Process the user selection
            // Numeric = 1 -> 7
            // Alpha = S or Q
            if (userSelection instanceof Integer) {
                // If the user entered a number, then we need to
                // process this selection. It is most likely a 
                // position on the board. 
                // 1 <= userSelection <= 7
                processNumericSelection((Integer)userSelection);
            } else {
                // If the user did NOT enter a number, they
                // must have entered a letter from the menu. 
                // They might want to Start/Restart or Quit
                option = (String)userSelection;
                processAlphaSelection();
            }
        }
    }

    /**
     * In this method, we need to figure out what the user wants
     * to do with the menu option that they selected. Notice that 
     * option is a global variable. We can assess what to do for 
     * a given input based on the option selected.
     * @param userSelection
     */
    public void processNumericSelection(Integer userSelection) {

        switch (userSelection) {

            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                option = String.valueOf(userSelection);
                if (!isGameOver())
                    playMove(userSelection);
                else
                {
                    message = "game Over";
                }
                break;
            default:
                message = ""; // set error message (for invalid selection?)
                break;
        }

    }


    /**
     * Process Alpha Selection
     * The user should have selected a letter from the menu. 
     * We need to process their selection.
     */
    private void processAlphaSelection() {
        String temp = option.toLowerCase();
        
        switch (temp) {
            case "s": // User selected option S - start/restart the game
                System.out.println("Game Started!");
                wipeBoard();
                gameOver = false;
                gameStarted = true;
                turn = "X";
                break;
            case "q": // User selected option 2 (time to go)
                System.out.println("Goodbye!");
                System.exit(0);
                break;
            default : // WTC
                System.out.println("Invalid Selection!");
                break;
        }
    }

    /**
     * Play Move
     * You will need to ensure that this column is not already full.
     * If not, place the proper game piece into the proper position 
     * on the board.
     * @param selection
     */
    public void playMove(int selection) {
        int row = selection-1;
        int top = getTopOfColumn(row);
        if (top > 0) {
            int space = (top * COL_COUNT) + row - COL_COUNT;
        
            board[space] = turn;
        }
        
        if (isGameOver() == false) {
            turn = (turn.equals("X")) ? "Y" : "X";
        }
        
    }
    
    /**
     * Is Game Over
     * In this method, we need to determine if all positions have
     * been filled or a user has connected four game pieces.
     * @return
     */
    private boolean isGameOver() {
        boolean allFull = true;
        for (int i=0; i<COL_COUNT; i++) {
            if (isColumnFull(i) == false) {
                allFull = false;
            }
        }
        if (allFull) {
            gameOver = true;
            return true;
        } else {
            //Check if a user has connected four pieces
            int xPos = 0;
            for (int yPos=0; yPos<ROW_COUNT; yPos++) {
                /**
                 * Checking a four-by four square for wins
                 */
                int corner = (yPos*COL_COUNT)+xPos;
                
                if (xPos < COL_COUNT-3) {
                    /**
                     * horizontal
                     * X X X X
                     * 
                     * 
                     * 
                     */
                    if (EMPTY_SPACE.equals(board[corner]) == false) {
                        if ( (board[corner].equals(board[corner + 1])) &&
                             (board[corner].equals(board[corner + 2])) &&
                             (board[corner].equals(board[corner + 3])) ) {
                            // horizontal is four-in-a-row, victory to the owner
                            winner = board[corner];
                            gameOver = true;
                            return true;
                        }
                    }
                }
                /**
                 * vertical
                 * X
                 * X
                 * X
                 * X
                 */
                if (yPos < ROW_COUNT-3) {
                    if (EMPTY_SPACE.equals(board[corner]) == false) {
                        if ( (board[corner].equals(board[corner + COL_COUNT])) &&
                             (board[corner].equals(board[corner+( COL_COUNT*2)])) &&
                             (board[corner].equals(board[corner+( COL_COUNT*3)])) ) {
                            // vertical is four-in-a-row, victory to the owner
                            winner = board[corner];
                            gameOver = true;
                            return true;
                        }
                    }
                }
                if ((xPos < COL_COUNT-3) && (yPos < ROW_COUNT-3)) {
                    /**
                     * downwards diagonal
                     * X
                     *   X
                     *     X
                     *       X
                     */
                    if (EMPTY_SPACE.equals(board[corner]) == false) {
                        if ( (board[corner].equals(board[corner+COL_COUNT+1])) &&
                             (board[corner].equals(board[corner+((COL_COUNT+1)*2)])) &&
                             (board[corner].equals(board[corner+((COL_COUNT+1)*3)])) ) {
                            // downwards diagonal is four-in-a-row, victory to the owner
                            winner = board[corner];
                            gameOver = true;
                            return true;
                        }
                    }
                    /**
                     * upwards diagonal
                     *       X
                     *     X
                     *   X
                     * X
                     */
                    if (EMPTY_SPACE.equals(board[corner + (ROW_COUNT*3)]) == false) {
                        if ( (board[corner + (ROW_COUNT*4)].equals(board[corner + (ROW_COUNT*2) + 1])) &&
                             (board[corner + (ROW_COUNT*4)].equals(board[corner + (ROW_COUNT*1)] + 2)) &&
                             (board[corner + (ROW_COUNT*4)].equals(board[corner + 3])) ) {
                            // upwards diagonal is four-in-a-row, victory to the owner
                            winner = board[corner + (ROW_COUNT*3)];
                            gameOver = true;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    private boolean isColumnFull(int col) {
        return (board[col] != EMPTY_SPACE);
    }
    
    private int getTopOfColumn(int col) {
        for (int i=0; i<ROW_COUNT; i++) {
            if (isSpaceFilled( ( i * COL_COUNT) + col )) {
                return i;
            }
        }
        return ROW_COUNT;
    }
    
    private boolean isSpaceFilled(int space) {
        return (board[space] != EMPTY_SPACE);
    }
    
    /**
     * Set Message
     * Our message variable is global. This method is global. We can
     * call this method from the Helper method when applicable.
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * Draw Board
     * This method draws the board in the console. The Helper method
     * calls a system command to clear the console. This does NOT work 
     * in Eclipse but works well in the Windows console. Check out the
     * document "Creating a runnable jar" in the Google Drive. If you run
     * your Connect Four application from a windows console, your console 
     * will clear successfully when the board is redrawn.
     */
    private void drawBoard() {
        // Clear the console, set messages in context, build the underscore string
        Helper.clearTheConsole();
        Helper.setMessage(option, gameOver, gameStarted, connectFour);

        // Do not remove the empty println statement. The clearTheConsole() function
        // call adds an odd character in Eclipse. We will build a new line.
        System.out.println("");

        // By default, the entire Connect Four board is drawn to the console.
        // You will need to provide a way to show X and O for each of the user's
        // selected moves.
        System.out.println("************* ConnectFour 1.0 ************* \n");
        System.out.println("                           1 2 3 4 5 6 7");
        System.out.println("                         __ _ _ _ _ _ _ __");
        System.out.print("Options:                 | ");
        drawRow(1);
        System.out.print("S. Start/Restart a game  | ");
        drawRow(2);
        System.out.print("Q. Quit                  | ");
        drawRow(3);
        System.out.print("                         | ");
        drawRow(4);
        System.out.print("                         | ");
        drawRow(5);
        System.out.print("TURN: "+turn+"                  | ");
        drawRow(6);
        System.out.print("                         + ============= +");
        System.out.println("");
        System.out.print("                         ||_____________||");
        System.out.println("");
        System.out.println(message);

        // Clear the message after we show it
        message = "";
    }

    /**
     * Draw ConnectFour Body Parts
     * The number of fails is global so we can use this information
     * in order to know how to draw the connectfour.
     * @param bodyPart
     */
    private void drawRow(int row) {
        switch (row) {
            case 1:
                for (int i = 0; i < 7; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
            case 2:
                for (int i = 7; i < 14; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
            case 3:
                for (int i = 14; i < 21; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
            case 4:
                for (int i = 21; i < 28; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
            case 5:
                for (int i = 28; i < 35; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
            case 6:
                for (int i = 35; i < 42; i++) {
                    System.out.print(board[i] + " ");
                }

                System.out.println("|");
                break;
        }
    }
    
    /**
     * wipeBoard - Clears the String array to represent an empty board.
     */
    public void wipeBoard() {
        //Initialize an empty board
        for (int i=0; i<SPACE_COUNT; i++) {
            board[i] = EMPTY_SPACE;
        }
    }

    /**
     * Main - Entry point into the application
     * @param args
     */
    public static void main(String[] args) {
        new ConnectFour();
    }
    
}